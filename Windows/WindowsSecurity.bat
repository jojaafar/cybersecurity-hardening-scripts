@echo off

REM Security Script Template for basic Windows Hardening. 

REM Install chocolatey and appropriate software
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
choco install ccleaner malwarebytes sysinternals mbsa


REM Disable default Guest and Admin
net user administrator /active:no
net user guest /active:no

REM Audit Policy
echo Setting up policy audits...
Auditpol /set /category:"Account Logon" /Success:enable /failure:enable
Auditpol /set /category:"Logon/Logoff" /Success:enable /failure:enable
Auditpol /set /category:"Account Management" /Success:enable /failure:enable
Auditpol /set /category:"DS Access" /Success:enable /failure:enable
Auditpol /set /category:"Object Access" /Success:enable /failure:enable
Auditpol /set /category:"policy change" /Success:enable /failure:enable
Auditpol /set /category:"Privilege use" /Success:enable /failure:enable
Auditpol /set /category:"System" /Success:enable /failure:enable
Auditpol /set /category:"Detailed Tracking" /Success:enable /failure:enable

REM Turn on firewall
netsh advfirewall setallprofiles state on 

REM setup password policies
echo Setting up password policies.
net accounts /forcelogoff:60 /lockoutthreshold:5 /MINPWLEN:8 /MAXPWAGE:30 /MINWPWAGE:1 /UNIQUEPW:5

REM Account Lockout Policy 
echo Setting up lockout policy...
net accounts /lockoutduration:30
net accounts /lockoutthreshold:5
net accounts /lockoutwindow:10

REM Clean out DNS cash
ipconfig /flushdns

REM Disable bad services
echo Disabling certain services.
net stop RemoteRegistry
sc config RemoteRegistry start = disabled
net stop SSDPSRV
sc config SSDPSRV start = disabled
net stop TlntSvr
sc config TlntSvr start = disabled
net stop SNMPTrap
sc config SNMPTrap start = disabled
net stop Spooler 
sc config Spooler start = disabled
net stop upnphost 
sc config upnphost start = disabled
net stop ftpsvc 
sc config ftpsvc start = disabled

REM Add registry keys for essential security options
echo Turn on automatic updating.
reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update" /v AUOptions /t REG_DWORD /d 4 /f

pause